const TARGET_WIDTH = 512;
const TARGET_HEIGHT = 512;
const FOOTER_HEIGHT = 72;
const DEACTIVATED_ICON_SIZE = 24;
const DEACTIVATED_ICON_PADDING = 24;
const FOOTER_BACKGROUND_COLOR = '#DDDDDD';
const FOOTER_TEXT_COLOR = '#7D7D7D';
const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/';

document.addEventListener('DOMContentLoaded', function () {
    const dropArea = document.getElementById('dragAreaLabel');
    const imageInput = document.getElementById('imageInput');
    const avatarURLInput = document.getElementById('avatarURLInput');
    const fileName = document.getElementById('fileName');
    const originalImage = document.getElementById('originalImage');
    const activatedAccountBlurry = document.getElementById('activatedAccountBlurry');
    const activatedAccount = document.getElementById('activatedAccount');
    
    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = TARGET_WIDTH;
    canvas.height = TARGET_HEIGHT;

    const footer = new Image();
    footer.src = './assets/footer.jpg';

    const deactivatedIcon = new Image();
    deactivatedIcon.src = './assets/deactivate_icon.svg';

    processBtn.addEventListener('click', function () {
        if (!avatarURLInput.value) return;
        let src = CORS_PROXY + avatarURLInput.value;
        activateAccount(src);
    })
    
    imageInput.addEventListener('change', function () {
        let src = URL.createObjectURL(imageInput.files[0]);
        activateAccount(src);
    });

    dropArea.addEventListener('dragover', function(e) {
        e.preventDefault();
    });

    dropArea.addEventListener('drop', function(e) {
        e.preventDefault();
        imageInput.files = e.dataTransfer.files;
        let src = URL.createObjectURL(imageInput.files[0]);
        activateAccount(src);
    });

    function activateAccount(src) {
        const image = new Image();
        image.src = src

        image.onload = function () {
            if (imageInput.files.length > 0) {
                fileName.textContent = imageInput.files[0].name;
            }
            if (image.width > image.height) {
                newWidth = TARGET_WIDTH;
                newHeight = (image.height / image.width) * TARGET_WIDTH;
            } else {
                newHeight = TARGET_HEIGHT;
                newWidth = (image.width / image.height) * TARGET_HEIGHT;
            }

            // Draw original image
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(image, (TARGET_WIDTH - newWidth) / 2, (TARGET_HEIGHT - newHeight) / 2, newWidth, newHeight);
            originalImage.src = canvas.toDataURL('image/png');
            
            // Convert to grey scale
            const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
            const data = imageData.data;
            for (let i = 0; i < data.length; i += 4) {
                const avg = (data[i] + data[i + 1] + data[i + 2]) / 3;
                data[i] = avg;
                data[i + 1] = avg;
                data[i + 2] = avg;
            }
            // Add blurry footer
            ctx.putImageData(imageData, 0, 0);
            ctx.drawImage(footer, 0, canvas.height - footer.height, canvas.width, footer.height);
            
            // Draw blurry image
            activatedAccountBlurry.src = canvas.toDataURL('image/png');
            
            // Render footer
            ctx.fillStyle = FOOTER_BACKGROUND_COLOR;
            ctx.fillRect(0, canvas.height - FOOTER_HEIGHT, canvas.width, FOOTER_HEIGHT);
            ctx.drawImage(deactivatedIcon, DEACTIVATED_ICON_PADDING, canvas.height - (FOOTER_HEIGHT + DEACTIVATED_ICON_SIZE) / 2, DEACTIVATED_ICON_SIZE, DEACTIVATED_ICON_SIZE);

            const text = 'Activated account';
            const x = DEACTIVATED_ICON_PADDING + DEACTIVATED_ICON_SIZE + 12;
            const y = canvas.height - (FOOTER_HEIGHT - DEACTIVATED_ICON_SIZE) / 2 - 5;
            const fontSize = '26px';
            const fontFamily = 'Lato, sans-serif';
            const fontWeight = 'bold';
            const fillStyle = FOOTER_TEXT_COLOR;
            ctx.font = `${fontWeight} ${fontSize} ${fontFamily}`;
            ctx.fillStyle = fillStyle;
            ctx.fillText(text, x, y);
            
            // Write rendered image
            activatedAccount.src = canvas.toDataURL('image/png');
        };
    }
});
